# Changelog

## 1.1.4

* Updated: More standards cleanup
* Updated: Settings library

## 1.1.2

* Updated: Settings library (fixes sysinfo security vulnerability)

## 1.1.1

* Fixed: I forgot how to SVN and broke all the things...

## 1.1.0

* Improved: Bring code up to reasonable standards

## 1.0.6

* Improved: Code refresh

## 1.0.5

* Added: Option to disable comments for posts by a given author

## 1.0.4

* Fixed: Update readme

## 1.0.3

* Improved: Prettify post type names

## 1.0.2

* Fixed: Update readme >_<

## 1.0.1

* Added: Option to globally disable based on post type
* Added: Option to globally disable based on user role
* Improved: Comment disable function

## 1.0.0

* Initial release
